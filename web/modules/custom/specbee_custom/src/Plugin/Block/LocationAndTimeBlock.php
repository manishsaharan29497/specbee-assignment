<?php

namespace Drupal\specbee_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\specbee_custom\Service\GetTime;

/**
 * Provides a 'Location and Time' Block.
 *
 * @Block(
 *   id = "location_and_time_block",
 *   admin_label = @Translation("Location and Time Block"),
 *   category = @Translation("Custom"),
 * )
 */
class LocationAndTimeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * Drupal\specbee_custom\Service.
   *
   * @var Drupal\specbee_custom\Service
   */
  private $getTime;

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('specbee_custom.get_time')
    );
  }

  /**
   * Constructor for the block.
   *
   * @param array $configuration
   *   Configuration array.
   * @param string $plugin_id
   *   String pluging id.
   * @param mixed $plugin_definition
   *   Mixed pluging defination.
   * @param Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configfactory.
   * @param Drupal\specbee_custom\Service\GetTime $getTime
   *   Custom service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, GetTime $getTime) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $configFactory;
    $this->getTime = $getTime;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $config = $this->config->get('custom_timezone.settings');

    $build = [
      'city' => $config->get('city'),
      'country' => $config->get('country'),
      'timezone' => $config->get('timezone'),
    ];

    return [
      '#theme' => 'site_location_settings',
      '#build' => $build,
      '#current_time' => $this->getTime->getCurrentTime(),
      '#cache' => [
        'tags' => ['config:custom_timezone.settings'],
      ],
    ];
  }

}
