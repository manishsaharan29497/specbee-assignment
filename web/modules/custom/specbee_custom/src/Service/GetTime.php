<?php

namespace Drupal\specbee_custom\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class getTime used get time dependent on timezone.
 *
 * @package \Drupal\specbee_custom\Service
 */
class GetTime {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory;
  }

  /**
   * Return current time.
   */
  public function getCurrentTime() {
    // Get timezone form config.
    $config = $this->config->get('custom_timezone.settings');
    $date = new \DateTime("now", new \DateTimeZone($config->get('timezone')));

    return $date->format('dS M Y - h:i:s A');
  }

}
